---
title: 网管培训之html标签
date: 2016-11-04 20:34:55
tags:
 - html
---
> 今天来介绍几个html标签

<!--more-->

```HTML
<!DOCTYPE html>
<html>
      <head>
            <meta http-equiv="content-type" content="text/html;charset=utf-8">
            <title></title>
      </head>
      <body>

      </body>
</html>
```
首先讲解一下主要的标签结构.
一个html文件要被包裹在一对html标签中,
html标签中分两个大块:head标签和body标签
## head标签里常用标签
先说head标签中写一些标签例如 meta,title,link,style,script......

      <meta> 元素可提供有关页面的元信息，比如针对搜索引擎和更新频度的描述和关键词.
                    这是官方的解释,我个人理解是meta就是用来解析页面, 例如告知浏览器html内文字的编码模式,http请求方式, 或者重定向到某个页面.

<br>

      <title> 元素可定义文档的标题.浏览器会以特殊的方式来使用标题,并且通常把它放置在浏览器窗口的标题栏或状态栏上.
                  同样,当把文档加入用户的链接列表或者收藏夹或书签列表时,标题将成为该文档链接的默认名称。

<br>

      <style> 写CSS样式的地方,可以从外部引入css样式文件例如:
                   <link rel="stylesheet" href="xxx.css" type="text/css" />
                   这就是link的作用,link还可以引用很多其他的文件,js,css.....都可以.

然后是body标签
在html页面里显示的内容都是body标签里的内容,所以要重点介绍一些写在body里的标签(其实基本大部分上标签都是写在这里的)
## hx标签

      <hx> 标签,定义几级标题的标签<h1>到<h6>共分六级(其实个人认为只是字体大小不一样而已,意义不大,
               建议自己用别的标签加样式自己用css调节).
```html
<h1>This is a heading</h1>
<h2>This is a heading</h2>
<h3>This is a heading</h3>
```

<br>
## p标签
      <p> 标签定义段落.会自成一段.

```html
<p>This is a paragraph.</p>
<p>This is another paragraph.</p>
```
## a标签

      <a> 标签定义超链接，用于从一张页面链接到另一张页面.

常用的属性有两个:
最重要的一个就是 href="www.xxx.com" 这个属性是用来定义要跳转的页面
另一个比较重要的属性就是 target=" "这个属性就是定义新页面打开的地方
target选项常用的是两个
_blank 在空白页面打开
_self  在自身页面打开
## img标签

      <img> 元素向网页中嵌入一幅图像。
                  请注意，从技术上讲,<img> 标签并不会在网页中插入图像,而是从网页上链接图像.
                  <img> 标签创建的是被引用图像的占位空间.


```html
<img src="XXX.jpg"  alt="XXX" height="100px" width="100px"/>
```
属性src是写入图片链接地址, alt是图片说明(就是如果没有加载出图片的话显示的文本)
height, width属性定义图片的大小不写的话默认是图片本身大小

## div标签

      <div> 标签可以把文档分割为独立的、不同的部分。它可以用作严格的组织工具，并且不使用任何格式与其关联。
                如果用 id 或 class 来标记 <div>，那么该标签的作用会变得更加有效。

```html
<div class="news">
  <h2>News headline 1</h2>
  <p>some text. some text. some text...</p>
  ...
 </div>
```
div标签主要用来打包一些小标签方便布局,这个标签很重要.

## table标签

      <table> 是形成表格的标签, 用法也比较简单

```html
<table>
      <tr><!--第一行表头-->
            <th>编号</th><!--表头标签-->
            <th>姓名</th>
            <th>年龄</th>
      </tr>
      <tr><!--第二行-->
            <td>1</td><!--数据行-->
            <td>张三</td>
            <td>18岁</td>
      </tr>
</table>
```
属性cellspacing="0px"一般都要写,因为默认样式两个表格之间都有一个空行会很难看
其余的属性我就不赘述了,用的时候可以自己查.

## ul li ol标签

      <ul> 标签定义无序列表.
      <ol> 标签定义有序列表.

```html
<ul>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>
<ol>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ol>
```
