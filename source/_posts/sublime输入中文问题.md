---
title: sublime text2 输入中文问题
date: 2017-01-22 12:51:39
tags:
  - ubuntu
---

> 使用sublime时发现无法输入中文,网上的一些方法有些问题,所以自己研究了一种修改方法,终于修改成功了

<!-- more -->

### 首先要建立一个sublime_imfix.c文件

```C
#include <gtk/gtkimcontext.h>
void gtk_im_context_set_client_window (GtkIMContext *context, GdkWindow *window)
{
       GtkIMContextClass *klass;
       g_return_if_fail (GTK_IS_IM_CONTEXT (context));
       klass = GTK_IM_CONTEXT_GET_CLASS (context);
       if (klass->set_client_window)
            klass->set_client_window (context, window);
       g_object_set_data(G_OBJECT(context),"window",window);
       if(!GDK_IS_WINDOW (window))
            return;
       int width = gdk_window_get_width(window);
       int height = gdk_window_get_height(window);
       if(width != 0 && height !=0)
            gtk_im_context_focus_in(context);
}

```

### 将上一步的代码编译成共享库libsublime-imfix.so

      gcc -shared -o libsublime-imfix.so sublime_imfix.c  `pkg-config --libs --cflags gtk+-2.0` -fPIC

### 然后将libsublime-imfix.so拷贝到sublime_text所在文件夹

      sudo mv libsublime-imfix.so /opt/sublime_text_2/

### 修改文件/usr/bin/subl的内容

```bash
#!/bin/bash

SUBLIME_HOME="/opt/sublime_text"
LD_LIB=$SUBLIME_HOME/libsublime-imfix.so
sh -c "LD_PRELOAD=$LD_LIB $SUBLIME_HOME/sublime_text $@"
```
### 最后修改/usr/share/applications/sublime_text.desktop里的所有Exec如图所示
![img](desktop.png)



### 成功示例

![img](success.png)
