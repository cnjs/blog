---
title: Ubunut的Apache2开启Rewirte模块
date: 2017-07-31 12:47:59
tags:
 - ubuntu
 - apache
 - php
---
> 最近在写一个自己的PHP框架,写到路由类的时候遇到了一个坑,就是Rewrite模块开启问题....

<!-- more -->

## 以下是我综合网上方法总结的解决办法:


在mods-available中可以看到名为rewrite.load的文件

### 1.加载以下命令开启Rewrite模块

```bash
sudo a2enmod rewrite
```

这时候发现mods-enabled目录下生已经成相应的链接

### 2.修改Apache的配置文件

```bash
sudo vim /etc/apache2/apache2.conf
```

将AllowOverride None 修改为 AllowOverride All

### 3.重启Apache即可
```bash
sudo /etc/init.d/apache2 restart
```
### 4.最后要提醒一点(很重要,本人就被这个坑了...)

写.htaccess的时候一定注意要是 *6个空格* 而不是一个tab

```
<IfModule mod_rewrite.c>
      RewriteEngine on
      RewriteCond %{REQUEST_FILENAME} !-f
      RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
</IfModule>

```