---
title: Ubuntu用hexo搭建博客方法
date: 2016-10-01 16:24:40
tags:
 - ubuntu
 - node.js
 - hexo

---


>  &nbsp;&nbsp;闲来无事本人自己研究了一下如何搭建一个博客,下面与大家分享一下我搭建的过程

<!--more-->
 #### 1.搭建node.js环境
  首先从源中安装node.js

      ```
      sudo apt-get install nodejs
      sudo apt-get install node
      sudo apt-get install npm
      ```

   然后安装版本控制 n

      ```
      sudo npm install -g n
      ```

   最后用 n 安装最新版本

      ```
      sudo n latest
      ```
 #### 2.搭建hexo博客
   安装hexo

      ```
      sudo npm install -g hexo
      ```
   新建一个文件夹,并初始化 例如:blog

      ```
      mkdir blog
      hexo init
      git init
      ```

   注册一个git账号,并在git上建一个仓库 名称为:**用户名**.github.io
   配置_config.yml, title啥的自己填, 主要是配置deploy, 注意空格和tab

      ```bash
      npm install hexo-deployer-git --save

      deploy:
        type: git
        repo:
          github: git@github.com:用户名/用户名.github.io.git,master
          coding: git@git.coding.net:用户名/用户名.git,master

      ```
   hexo g 生成文件
   hexo s 本地测试
   hexo d 上传到服务器

   解决GitHub Clone慢 Push慢 Pull慢
   解决办法,在host文件中添加两地址
   vim /etc/hosts
   ```
   151.101.113.194 github.global.ssl.fastly.net
   192.30.253.113 github.com
   ```
