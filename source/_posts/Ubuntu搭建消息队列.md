---
title: Ubuntu搭建消息队列
date: 2019-01-27 00:48:42
tags:
---

> Ubuntu 16.04 搭建消息队列的一个教程, 通过网上整理的笔记

<!-- more -->

## 一.安装RabbitMQ

```bash
echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
wget -O - 'https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc' | sudo apt-key add -
sudo apt-get update
sudo apt-get install rabbitmq-server
```

## 二.配置并启动RabbitMQ

1.启动消息队列

```bash
service rabbitmq-server start
```
2.查看RabbitMQ

```bash
service rabbitmq-server status
```
![img](1.png)

3.解决User can only log in via localhost(说白了就是解远端登录问题)

这个时候，需要新建/etc/rabbitmq/rabbitmq.config文件

```bash
service rabbitmq-server stop
vim /etc/rabbitmq/rabbitmq.config
```
文件内写入

```
[
{rabbit, [{tcp_listeners, [5672]}, {loopback_users, ["admin"]}]}
].
```
这样就兼容远端访问了.

4.重新启动消息队列,并且开启web访问
```bash
service rabbitmq-server start
rabbitmq-plugins enable rabbitmq_management
```
本地登录 http://localhost:15672
远端登录 http://外网ip:15672

用户名: guest
密码: guest

![img](2.png)
