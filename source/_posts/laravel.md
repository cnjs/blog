---
title: laravel环境搭建
date: 2016-10-15 18:01:51
tags:
 - php
 - laravel
---
> laravel的折腾之路
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------本文 部分搬运自liufang169的博客

<!--more-->
## PHP环境搭建
ubuntu 16.04

      sudo apt-get install php7.0
      sudo apt-get install php7.0-zip

ubuntu 14.04

      sudo apt-get install php5
      sudo apt-get install php5-zip

## nodejs环境搭建
请见 https://syjs10.github.io/2016/10/01/blog/ 中的nodejs环境搭建.


## 安装composer

composer在PHP中的地位相当于, Python中的pip, Nodejs中的npm, 用于安装一些大神做好的插件.
### 1.首先先下载

      curl -sS https://getcomposer.org/installer | php
      mv composer.phar /usr/local/bin/composer

### 2.安装laravel

      composer global require "laravel/installer"

这个命令是在 ~/.composer/vendor/安装了laravel 的等等cli程序，所以要配置环境变量.

      sudo vim /etc/profile

我在profile文件最后一行加上了：export PATH="~/.composer/vendor/bin:vendor/bin:$PATH"

### 3.注意可能ubuntu14.04会出现php版本过低导致安装laravel

      sudo apt-get install python-software-properties

      sudo add-apt-repository ppa:ondrej/php5-5.6

      sudo apt-get update

      sudo apt-get install php5


## 新建项目

      mkdir xxx && cd xxx

      laravel new xxx

### 启动laravel

      sudo chmod 777 artisan && php artisan serve
