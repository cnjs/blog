---
title: Python中class继承dict之后的变化
date: 2017-05-23 20:26:42
tags: 
  - python
---
> 学习廖雪峰大神的Python教程时遇到的一个问题，尝试了一下得到的一些结论，总结梳理一下.....

<!--more-->
首先说一下问题：

```python
class Model (dict, metaclass=ModelMetaclass):
	def __init__(self, **kw):
		super(Model, self).__init__(**kw)

	def __getattr__(self, key):
		try:
			return self[key]
		except KeyError:
			raise AttributeError(r"'Model' object has no attribute '%s'" % key)
	def __setattr__(self, key, value):
		self[key] = value

```

这是web实战的数据库部分的一段代码，我就比较好奇为什么可以用self访问**kw里的内容

于是我写了一个demo

```python
class test (dict):
	def __init__(self, **kw):
		super(test, self).__init__(**kw)
		print(self)
test(name='JS')

```

得到的结果是这样的

	{'name':'JS'}

于是我猜想：self本身是一个字典吗，难道所有属性和方法都是以字典的方式存储的吗？

```python
class test (dict):
	def __init__(self, name):
		self['name'] = name
		print(self)
test(name='JS')

```
得到的结果是这样的

	{'name':'JS'}

我以为我的猜想对了

可是后来我一想，诶不对啊我貌似继承了dict啊
于是我又做了个测试

```python
class test ():
	def __init__(self):
		print(self)
test()

```
得到的结果是这样的

	<__main__.test object at 0x7f51c815ba20>

诶 class是object 那dict是什么呢，于是我开始上网查找.....

原来object是一个整体性的对数据的抽象概念，而dict是object的一种具体形式，就如同int float list tuple set都是object的子类,所以我们继承了dict之后self变成了一个dict，但是self本身的类型应该是object。